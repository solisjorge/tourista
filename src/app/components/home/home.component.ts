import { Component, OnInit } from '@angular/core';
import { Movie } from '../../model/dto';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

	public movieList:Movie[] = [];

  constructor(private httpClient:HttpClient) {
  	
  }

  ngOnInit() {
  	//console.log('LLama a http://www.omdbapi.com/?apikey=f12ba140&s=Star%20Wars')
  	this.httpClient.get<Movie[]>('http://www.omdbapi.com/?apikey=f12ba140&s=Star%20Wars')
  		.subscribe(result=>this.onMovies(result))
  }

  onMovies(result){
  	var tot = result['Search']['length']
  	var movie:Movie;
  	for (var i = 0; i < tot; i++) {
  		movie = result['Search'][i];
  		this.movieList.push(movie);
  	}
  }
}

